# 8INF843 - Distributed Systems

The aim to this project was to used RMI (Java Remote Invocation) in order to distribute calculation operation to a server.

## Requirements

* Java >= 8
* Gradle

## To Do

- [x] Project setup with Gradle
- [x] Added Java classes provided in the project
- [x] Build client/server interface
- [x] Send request to server - parameters : string
- [x] Send request to server - parameters : serialized class
- [x] Save serialized class to temporary object and send it to server
- [x] Fetch dynamically method parameters
- [x] Refactoring

## Setup

* Clone or download this repository
* Configure this project with your favorite IDE
* Run the the server first. Then, start the client!
* Enjoy! :smile:
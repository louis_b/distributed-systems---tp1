package uqac;

import java.io.Serializable;

/**
 * The type Calc.
 */
public class Calc implements Serializable {
    /**
     * Add int.
     *
     * @param a the a
     * @param b the b
     * @return the int
     */
    public int add(String a, String b) {
        int x = Integer.parseInt(a);
        int y = Integer.parseInt(b);
        return x + y;
    }
}

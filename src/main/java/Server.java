import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;


/**
 * The type Server.
 */
public class Server extends UnicastRemoteObject implements RMIInterface {

    /**
     * Instantiates a new Server.
     *
     * @throws RemoteException the remote exception
     */
    protected Server() throws RemoteException {
        super();
    }

    /**
     *
     * @param serializedPath the serialized path
     * @param methodToRun    the method to run
     * @return
     * @throws RemoteException
     */
    @Override
    public String runMethod(String serializedPath, String methodToRun) throws RemoteException {
        // split command to execute
        String[] parts = methodToRun.split(Constants.COMMAND_SEPARATOR);

        // if command exist, handle it
        if (parts.length >= 2) {
            String className = parts[0];
            String methodName = parts[1];
            String params = parts[2];

            // dynamically fetch types of all parameters
            Object[] allParams = params.split(Constants.PARAMS_SEPARATOR);
            Class<?>[] pType = null;
            try {
                Class<?> clazz = Class.forName(Constants.CLASSES_PACKAGE + className);
                Method[] allMethods = clazz.getDeclaredMethods();
                for (Method m : allMethods) {
                    if (!m.getName().equals(methodName)) {
                        continue;
                    }
                    pType = m.getParameterTypes();
                }

                // execute method
                Method method2 = clazz.getMethod(methodName, pType);
                // send method to client
                return method2.invoke(ByteStream.deserializeObject(serializedPath), allParams).toString();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return "The method to execute is " + methodToRun;
    }

    /**
     * Main.
     *
     * @param args the args
     */
    public static void main(String[] args) {
        try {
            // create registry
            LocateRegistry.createRegistry(1099);
            // start server
            Naming.rebind(Constants.RMI_SERVER_URL, new Server());
            System.err.println("Server ready");
        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

}
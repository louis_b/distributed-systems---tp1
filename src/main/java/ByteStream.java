import org.apache.commons.lang3.RandomStringUtils;

import java.io.*;

/**
 * The type Byte stream.
 */
public class ByteStream {

    /**
     * Serialize object string.
     *
     * @param object the object
     * @return the string
     */
    public static String serializeObject(Object object) {
        try {
            String generatedString = RandomStringUtils.randomAlphabetic(10);
            String path = "/tmp/" + generatedString + ".ser";

            FileOutputStream fileOut =
                    new FileOutputStream(path);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(object);
            out.close();
            fileOut.close();

            return path;
        } catch (IOException i) {
            i.printStackTrace();
        }

        return null;
    }

    /**
     * Deserialize object object.
     *
     * @param objectTmpString the object tmp string
     * @return the object
     */
    public static Object deserializeObject(String objectTmpString) {
        FileInputStream fileIn = null;
        Object object = null;
        try {
            fileIn = new FileInputStream(objectTmpString);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            object = in.readObject();
            in.close();
            fileIn.close();

            return object;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return object;
    }
}

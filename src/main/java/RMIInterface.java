import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * The interface Rmi interface.
 */
public interface RMIInterface extends Remote {

    /**
     * Run method string.
     *
     * @param serializedPath the serialized path
     * @param methodToRun    the method to run
     * @return the string
     * @throws RemoteException the remote exception
     */
    public String runMethod(String serializedPath, String methodToRun) throws RemoteException;

}
import uqac.Calc;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import javax.swing.JOptionPane;

/**
 * The type Client.
 */
public class Client {

    private static RMIInterface server;

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     * @throws MalformedURLException the malformed url exception
     * @throws RemoteException       the remote exception
     * @throws NotBoundException     the not bound exception
     */
    public static void main(String[] args) throws MalformedURLException, RemoteException, NotBoundException {
        server = (RMIInterface) Naming.lookup(Constants.RMI_SERVER_URL);

        // instantiate the Calc class and serializes it
        Calc calc = new Calc();
        String path = ByteStream.serializeObject(calc);

        // send it to server
        String response = server.runMethod(path, "Calc&add&20,15");
        // display result
        JOptionPane.showMessageDialog(null, "Le résultat est : " + response);
    }

}

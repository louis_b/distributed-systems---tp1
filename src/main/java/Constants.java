import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * The type Constants.
 */
public final class Constants {

    private Constants() {
        // restrict instantiation
    }

    /**
     * The constant RMI_SERVER_URL.
     */
    public static String RMI_SERVER_URL = null;
    static {
        try {
            RMI_SERVER_URL = "rmi://" + InetAddress.getLocalHost().getHostAddress() + "/TestRMI";
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

    /**
     * The constant METHOD_SEPARATOR.
     */
    public static final String COMMAND_SEPARATOR = "&";
    /**
     * The constant PARAMS_SEPARATOR.
     */
    public static final String PARAMS_SEPARATOR = ",";
    /**
     * The constant CLASSES_PACKAGE.
     */
    public static final String CLASSES_PACKAGE = "uqac.";

}
